# Day2DayDeployment

This repository implements the code for the day2day public ec2 instance which listens to the public requests and based on some metrics scales the ML Auto Scaling Group and provides the data to the ASG through AWS SQS service.
