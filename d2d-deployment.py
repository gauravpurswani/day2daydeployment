import boto3
import datetime as dt
from flask import Flask, redirect, url_for, request
import time
import requests
import pymongo

app = Flask(__name__)

def log(msg):
	f = open(r'/home/pingport80/.dtdlogs', 'a')
	f.write(str(dt.datetime.now()) + ": " + msg)
	f.write('\n')
	f.close()


cloudwatch = boto3.client('cloudwatch')
ec2 = boto3.resource('ec2')
asg_client = boto3.client('autoscaling')

time_zero = dt.datetime.strptime('0:00:00', '%H:%M:%S')

next_instance = -1
waiting = False

ec2_instances = [] # list of currently running ec2 instance_Ids
video_duration = {} # dict - {s3_link:video_duration}
loadPerInstance = {} # dict - the total video duration each instance is handling - {instance_id:total_duration}

client = pymongo.MongoClient("mongoDBAdress")
db = client["databaseName"]
collection = db["tableName"]

def getInstance():
	global waiting
	for instance in ec2_instances:
		if ((loadPerInstance[instance] - time_zero).total_seconds() // 3600) < 10:
			return instance
	if len(ec2_instances) < 3:
		# launch and register instance
		waiting = True
		scale_up_alarm()
		waiting = False
		print('getInstance '+ec2_instances[-1])
		return ec2_instances[-1]
	else:
		global next_instance
		log('Maximum instances reached. Cannot scale out more!')
		next_instance = (next_instance + 1) % len(ec2_instances)
		return ec2_instances[next_instance]


def registerRunningInstances():
	asg_response = asg_client.describe_auto_scaling_groups(AutoScalingGroupNames=['asgtest'])
	instances = asg_response['AutoScalingGroups'][0]['Instances']
	for instance in instances:
		print(instance['InstanceId'])
		log("Already Running instance registered with " +instance['InstanceId'])
		instance_id = instance['InstanceId']
		ec2_instances.append(instance_id)
		loadPerInstance[instance_id] = dt.datetime.strptime('0:00:00', '%H:%M:%S')

def deregisterInstance(instance_id):
	ec2_instances.remove(instance_id)
	loadPerInstance.pop(instance_id)
	log("Instance deregistered with Instance_Id: "+instance_id)

def registerNewInstance(instance_id):
	ec2_instances.append(instance_id)
	loadPerInstance[instance_id] = dt.datetime.strptime('0:00:00', '%H:%M:%S')	
	log("New instance registered with Instance_Id: "+instance_id)


# method to change the state of a alarm
def set_alarm(alarm_name, state):
    response = cloudwatch.set_alarm_state(
    AlarmName=alarm_name,
    StateValue=state,
    StateReason='scaling'
    )

# will trigger the scale_up_alarm and the ASG will launch a new instance
def scale_up_alarm():
    set_alarm('scale_up_alarm', "ALARM")
    log("Scale Up Event")
    time.sleep(120)
    asg_response = asg_client.describe_auto_scaling_groups(AutoScalingGroupNames=['asgtest'])
    instances = asg_response['AutoScalingGroups'][0]['Instances']
    for instance in instances:
    	if instance['InstanceId'] not in ec2_instances:
    		registerNewInstance(instance['InstanceId'])
    		print(instance['InstanceId'])


# will trigger the scale_in alarm and the ASG will terminate and detach a given instance
def scale_down(instance_id):
	log("Scale Down Event")
	instance = ec2.Instance(instance_id)
	log("Detaching instance from the ASG with Instance_Id: "+instance_id)
	asg_client.detach_instances(
		ShouldDecrementDesiredCapacity = True,
		AutoScalingGroupName = 'asgtest',
		InstanceIds = [instance_id]
	)
	log("terminating instance with Instance_Id: "+instance_id)
	instance.terminate()

def sendAPI(s3_link, instance_id, excercise_id):
	instance = ec2.Instance(instance_id)
	print('sending request to '+instance_id)
	#private_ip_address = instance.private_ip_address()
	ip_address = instance.public_ip_address
	print(ip_address)
	requests.get('http://'+ip_address+'/?s3_link='+s3_link+'&id='+str(excercise_id))
	requestStatus = { "s3_link": s3_link, "excercise_id": excercise_id, "status": "requestDispatched" }
	#collection.insert_one(requestStatus)
	loadPerInstance[instance_id] = loadPerInstance[instance_id] - time_zero + video_duration[s3_link]


#####################################################################################################
#below code always listens to the request and performs the following task on each request

@app.route('/')
def index():
	global waiting
	# duration = getDuration(s3_link)
	print(ec2_instances)
	duration = dt.datetime.strptime(request.args.get('duration'),"%H:%M:%S")
	excercise_id = request.args.get('id')
	s3_link = request.args.get('s3_link')
	video_duration[s3_link] = duration
	print(video_duration)

	while waiting:
		time.sleep(20)

	instance_id = getInstance()
	print(instance_id)

	sendAPI(s3_link, instance_id, excercise_id)
	print(loadPerInstance)
	log("API sent for " + s3_link + " which is " + str(video_duration[s3_link]) + " long to " + instance_id)
	return 'successfully despatched the video'

#######################################################################################################




#######################################################################################################
# API for reducing the total_duration when the video is processed completely

@app.route('/notify_completion')
def notify_completion():
	s3_link = request.args.get('s3_link')
	instance_id = request.args.get('instance_Id')
	log('Video Processing Completed for '+ s3_link + " by " + instance_id)
	duration = video_duration[s3_link]
	#total_duration = total_duration - duration # this will convert the total_duration to time delta object
	#total_duration = time_zero + total_duration # converting time delta object back to datetime 
	video_duration.pop(s3_link)
	loadPerInstance[instance_id] = loadPerInstance[instance_id] - duration + time_zero
	if len(ec2_instances) > 1 and loadPerInstance[instance_id] == time_zero:
		scale_down(instance_id)
		deregisterInstance(instance_id)
	print(loadPerInstance)
	return ''
########################################################################################################


print('---start----')
registerRunningInstances()
print(ec2_instances)
app.run()
